﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioVariation : MonoBehaviour
{
    private AudioSource src;

    private float initialPitch;
    public float pitchVariation = 0.1f;

    // Start is called before the first frame update
    void Start()
    {
        src = GetComponent<AudioSource>();
        initialPitch = src.pitch;
    }

    public void Play()
    {
        Debug.Log("Button pressed");
        src.pitch = initialPitch + Random.Range(-pitchVariation, pitchVariation);

        src.Play();
    }
}